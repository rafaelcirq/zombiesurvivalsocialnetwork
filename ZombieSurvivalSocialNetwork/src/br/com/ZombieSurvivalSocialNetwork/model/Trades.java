package br.com.ZombieSurvivalSocialNetwork.model;

import java.util.ArrayList;

/*
 * responsible for holding trades infos, but trades are not working
 */
public class Trades {
	private ArrayList<Resources> resourcesToTradeSurvivor1;
	private ArrayList<Resources>  resourcesToTradeSurvivor2;
	private int idSurvivor1;
	
	public int getIdSurvivor1() {
		return idSurvivor1;
	}
	public void setIdSurvivor1(int idSurvivor1) {
		this.idSurvivor1 = idSurvivor1;
	}
	public int getIdSurvivor2() {
		return idSurvivor2;
	}
	public void setIdSurvivor2(int idSurvivor2) {
		this.idSurvivor2 = idSurvivor2;
	}
	private int idSurvivor2;
	
	public ArrayList<Resources> getResourcesToTradeSurvivor1() {
		return resourcesToTradeSurvivor1;
	}
	public void setResourcesToTradeSurvivor1(ArrayList<Resources> resourcesToTradeSurvivor1) {
		this.resourcesToTradeSurvivor1 = resourcesToTradeSurvivor1;
	}
	public ArrayList<Resources> getResourcesToTradeSurvivor2() {
		return resourcesToTradeSurvivor2;
	}
	public void setResourcesToTradeSurvivor2(ArrayList<Resources> resourcesToTradeSurvivor2) {
		this.resourcesToTradeSurvivor2 = resourcesToTradeSurvivor2;
	}
}
