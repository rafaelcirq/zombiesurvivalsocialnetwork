package br.com.ZombieSurvivalSocialNetwork.model;

/*
 * all the avaiable types of resources
 */
public enum Resources {
	WATER("Water", 4),
	FOOD("Food", 3),
	MEDICATION("Medication", 2),
	AMMUNITION("Ammunition", 1);
	
	private int points;
	private int quantity=0;
	private String name;
	
	@Override
	public String toString() {
		return this.getName()+"-"+this.getQuantity();
	}
	
	Resources(String name, int points)
	{
		this.name = name;
		this.points = points;
	}
	
	public int getPoints()
	{
		return this.points;
	}
	public String getName()
	{
		return this.name();
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
}
