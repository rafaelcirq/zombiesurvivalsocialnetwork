package br.com.ZombieSurvivalSocialNetwork.model;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlRootElement;

/*
 * Class responsible for holding the survivors infos
 */
@XmlRootElement
public final class Survivors {
	private Integer id;
	private String name;
	private Integer age;
	private String gender;
	private String location;
	private boolean infected;
	private ArrayList<Resources> resourcesList;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public boolean isInfected() {
		return infected;
	}
	public void setInfected(boolean infected) {
		this.infected = infected;
	}
	public ArrayList<Resources> getResourcesList() {
		return resourcesList;
	}
	public void setResourcesList(ArrayList<Resources> resourcesList) {
		this.resourcesList = resourcesList;
	}
}
