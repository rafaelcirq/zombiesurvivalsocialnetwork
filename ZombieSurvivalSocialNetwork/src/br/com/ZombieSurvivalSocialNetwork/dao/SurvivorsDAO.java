package br.com.ZombieSurvivalSocialNetwork.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import br.com.ZombieSurvivalSocialNetwork.factory.ConnectionFactory;
import br.com.ZombieSurvivalSocialNetwork.model.Resources;
import br.com.ZombieSurvivalSocialNetwork.model.Survivors;

/*
 * responsible for holding the methods related to database
 */
public class SurvivorsDAO extends ConnectionFactory {
	
	private static SurvivorsDAO instance;
	
	public static SurvivorsDAO getInstance()
	{
		if(instance==null)
		{
			instance = new SurvivorsDAO();
		}
		
		return instance;
	}
	
	public ArrayList<Resources> getAllResources(Integer id)
	{
		ArrayList<Resources> resourcesList = new ArrayList<>();
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		con = createConnection();
		ArrayList<String> resourcesInString = new ArrayList<>();
		try {
			if(id!=null)
			{
				ps = con.prepareStatement("select resources from "
						+ "survivors where id = "+id+";");
			}
			else
			{
				ps = con.prepareStatement("select resources from survivors");
			}
			rs = ps.executeQuery();
			while(rs.next())
			{
				resourcesInString.add(rs.getString("resources"));
			}
			resourcesList=this.getResourcesFromString(resourcesInString);
		} catch (Exception e) {
			System.out.println("Error, can't list survivors. "+e.getMessage());
		} finally {
			closeConnection(con, ps, rs);
		}
		return resourcesList;
	}
	
	private ArrayList<Resources> getResourcesFromString(ArrayList<String> resourcesInString)
	{
		ArrayList<Resources> resourcesList = new ArrayList<>();
		resourcesList.add(Resources.AMMUNITION);
		resourcesList.add(Resources.FOOD);
		resourcesList.add(Resources.MEDICATION);
		resourcesList.add(Resources.WATER);
		for (Resources resource : resourcesList) {
			resource.setQuantity(0);
		}
		for (String resourceInString : resourcesInString) {
			String[] splitedLine = resourceInString.split(",");
			for (String resource : splitedLine) {
				String[] values = resource.split("-");
				values[0]=values[0].substring(1);
				if(values[1].substring(values[1].length()-1, values[1].length()).equals("]"))
				{
					values[1]=values[1].substring(0, values[1].length()-1);
				}
				for (Resources resourceFromList : resourcesList) {
					if(resourceFromList.getName().equals(values[0]))
					{
						int quantity = Integer.parseInt(values[1]);
						resourceFromList.setQuantity(resourceFromList.getQuantity()+quantity);
					}
				}
			}
		}
		
		return resourcesList;
	}
	
	public String getAverageAmountsOfResource()
	{
		ArrayList<Resources> list = this.getAllResources(null);
		StringBuilder stringReturn = new StringBuilder();
		
		for (Resources resource : list) {
			double average = 0;
			int totalSurvivors = this.getSurvivors(null).size();
			average = resource.getQuantity()/totalSurvivors;
			stringReturn.append(average+" "+resource.getName()+"S per survivor\n");
		}
		
		return stringReturn.toString();
	}
	
	public void takeTradedResources(ArrayList<Resources> tradedResources, int id)
	{
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		con = createConnection();
		
		ArrayList<Resources> dataBaseResources = this.getAllResources(id);
		for (Resources dataBaseResource : dataBaseResources) {
			for (Resources tradedResource : tradedResources) {
				if(dataBaseResource.getName().equals(tradedResource.getName()))
				{
					dataBaseResource.setQuantity(dataBaseResource.getQuantity()-
							tradedResource.getQuantity());
				}
			}
		}
		
		try {
			ps = con.prepareStatement("UPDATE survivors SET resources = '"+dataBaseResources
					+ "' WHERE id = "+id+";");
			ps.executeUpdate();
		} catch (Exception e) {
			System.out.println("Error, can't update. "+e.getMessage());
		} finally {
			closeConnection(con, ps, rs);
		}
	}
	
	public void addResources (ArrayList<Resources> tradedResources, int id)
	{
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		con = createConnection();
		
		ArrayList<Resources> dataBaseResources = this.getAllResources(id);
		for (Resources dataBaseResource : dataBaseResources) {
			for (Resources tradedResource : tradedResources) {
				if(dataBaseResource.getName().equals(tradedResource.getName()))
				{
					dataBaseResource.setQuantity(dataBaseResource.getQuantity()+
							tradedResource.getQuantity());
				}
			}
		}
		
		try {
			ps = con.prepareStatement("UPDATE survivors SET resources = '"+dataBaseResources
					+ "' WHERE id = "+id+";");
			ps.executeUpdate();
		} catch (Exception e) {
			System.out.println("Error, can't update. "+e.getMessage());
		} finally {
			closeConnection(con, ps, rs);
		}
	}
	
	public void updateInfectedStatus(int id)
	{
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		con = createConnection();
		
		try {
			ps = con.prepareStatement("UPDATE survivors SET infected = '"+true
					+ "' WHERE id = "+id+";");
			ps.executeUpdate();
		} catch (Exception e) {
			System.out.println("Error, can't add survivors. "+e.getMessage());
		} finally {
			closeConnection(con, ps, rs);
		}
	}
	
	/*
	 * this method gets a list with all survivors
	 * @return ArrayList<Survivors> survivorsList
	 */
	public ArrayList<Survivors> getSurvivors(Integer id)
	{
		ArrayList<Survivors> survivorsList = new ArrayList<>();
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		con = createConnection();
		
		try {
			if(id!=null)
			{
				ps = con.prepareStatement("select * from survivors where id = "+id+";");
			}
			else
			{
				ps = con.prepareStatement("select * from survivors"+";");
			}
			rs = ps.executeQuery();
			while(rs.next())
			{
				Survivors survivor = new Survivors();
				survivor.setId(rs.getInt("id"));
				survivor.setName(rs.getString("name"));
				survivor.setAge(rs.getInt("age"));
				survivor.setGender(rs.getString("gender"));
				survivor.setLocation(rs.getString("location"));
				survivor.setInfected(rs.getBoolean("infected"));
				
				survivorsList.add(survivor);
			}
		} catch (Exception e) {
			System.out.println("Error, can't list survivors. "+e.getMessage());
		} finally {
			closeConnection(con, ps, rs);
		}
		
		return survivorsList;
	}
	
	public void updateSurvivorLocation(int id, String newLocation)
	{
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		con = createConnection();
		
		try {
			ps = con.prepareStatement("UPDATE survivors SET location = '"+newLocation
					+ "' WHERE id = "+id+";");
			ps.executeUpdate();
		} catch (Exception e) {
			System.out.println("Error, can't add survivors. "+e.getMessage());
		} finally {
			closeConnection(con, ps, rs);
		}
	}
	
	public void addNewSurvivor(Survivors newSurvivor)
	{
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		con = createConnection();
		
		StringBuilder columns = new StringBuilder();
		columns.append("(");
		columns.append("name, ");
		columns.append("age, ");
		columns.append("gender, ");
		columns.append("location, ");
		columns.append("infected, ");
		columns.append("resources");
		columns.append(")");
		
		StringBuilder info = new StringBuilder();
		info.append("(");
		info.append("'"+newSurvivor.getName()+"',");
		info.append(" "+newSurvivor.getAge()+",");
		info.append(" '"+newSurvivor.getGender()+"',");
		info.append(" '"+newSurvivor.getLocation()+"',");
		info.append(" "+newSurvivor.isInfected()+",");
		info.append(" '"+newSurvivor.getResourcesList().toString()+"'");
		info.append(")");
		
		try {
			ps = con.prepareStatement("insert into survivors "+columns.toString()
					+ " values "+info.toString()+";");
			ps.executeUpdate();
		} catch (Exception e) {
			System.out.println("Error, can't add survivors. "+e.getMessage());
		} finally {
			closeConnection(con, ps, rs);
		}
	}
	
	public void removeSurvivor(int id)
	{
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		con = createConnection();
		
		try {
			ps = con.prepareStatement("delete from survivors where id = "+id+";");
			ps.executeUpdate();
		} catch (Exception e) {
			System.out.println("Error, can't remove survivor. "+e.getMessage());
		} finally {
			closeConnection(con, ps, rs);
		}
	}
	
	
	public double percentageOfInfectedSurvivors()
	{
		
		ArrayList<Survivors> survivorsList = getSurvivors(null);
		int totalSurvivors = survivorsList.size();
		int infectedSurvivors = 0;
		
		for (Survivors survivor : survivorsList) {
			if(survivor.isInfected())
			{
				infectedSurvivors++;
			}
		}
		
		return (100*infectedSurvivors)/totalSurvivors;
	}
	
	public double percentageOfNonInfectedSurvivors()
	{
		
		ArrayList<Survivors> survivorsList = getSurvivors(null);
		int totalSurvivors = survivorsList.size();
		int nonInfectdSurvivors = 0;
		
		for (Survivors survivor : survivorsList) {
			if(!survivor.isInfected())
			{
				nonInfectdSurvivors++;
			}
		}
		
		return (100*nonInfectdSurvivors)/totalSurvivors;
	}
	
/*	public Integer listLostPoints()
	{
		
	}*/
}
