package br.com.ZombieSurvivalSocialNetwork.resource;

import java.util.ArrayList;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import br.com.ZombieSurvivalSocialNetwork.controller.SurvivorsController;

/*
 * holds the Rest methods to access webservice
 */
@Path("/survivors")
public class SurvivorResource {
	
	@GET
	@Path("/getPercentageOfInfectedSurvivors")
	@Produces("application/json")
	public String percentageOfInfectedSurvivors()
	{
		return new SurvivorsController().getPercentageOfInfectedSurvivors();
	}
	
	@GET
	@Path("/getPercentageOfNonInfectedSurvivors")
	@Produces("application/json")
	public String percentageOfNonInfectedSurvivors()
	{
		return new SurvivorsController().getPercentageOfNonInfectedSurvivors();
	}
	
	@GET
	@Path("/getAverageAmountOfResources")
	@Produces("application/json")
	public String getAverageAmountOfResources ()
	{
		return new SurvivorsController().getAverageAmountOfResources();
	}

}
