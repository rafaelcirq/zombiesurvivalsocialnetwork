package br.com.ZombieSurvivalSocialNetwork.factory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/*
 * contains methods responsible for creating and closing database
 */
public class ConnectionFactory {
	private static final String DRIVER = "org.postgresql.Driver";
	private static final String URL = "jdbc:postgresql://localhost:5433/zssn";
	private static final String USER = "postgres";
	private static final String PASSWORD = "";
	
	/*
	 * this method creates connection to database
	 */
	public Connection createConnection()
	{
		Connection con = null;
		
		try {
			Class.forName(DRIVER);
			con = DriverManager.getConnection(URL, USER, PASSWORD);
			
		} catch (Exception e) {
			System.out.println("Error by creating connection");
		}
		
		return con;
	}
	
	public void closeConnection(Connection con, PreparedStatement ps, ResultSet rs)
	{
		try {
			if(con!= null)
			{
				con.close();
			}
			if(ps!=null)
			{
				ps.close();
			}
			if(rs!=null)
			{
				rs.close();
			}
		} catch (Exception e) {
			System.out.println("Error by closing connection"+URL);
		}
	}
}
