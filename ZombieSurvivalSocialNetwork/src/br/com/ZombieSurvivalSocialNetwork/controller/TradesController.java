package br.com.ZombieSurvivalSocialNetwork.controller;

import java.util.ArrayList;

import br.com.ZombieSurvivalSocialNetwork.dao.SurvivorsDAO;
import br.com.ZombieSurvivalSocialNetwork.model.Resources;
import br.com.ZombieSurvivalSocialNetwork.model.Survivors;
import br.com.ZombieSurvivalSocialNetwork.model.Trades;

/*
 * responsible for holding the trades methods, but trades are not working
 */
public class TradesController {

	private Trades trade = new Trades();
	
	private Trades getTrade() {
		return trade;
	}

	private void setTrade(Trades trade) {
		this.trade = trade;
	}

	private int calculaPoints (ArrayList<Resources> resourcesList)
	{
		int points=0;
		for (Resources resource : resourcesList) {
			points=points+(resource.getPoints()*resource.getQuantity());
		}
		return points;
	}
	
	public void setData(int idSurvivor1, int idSurvivor2, 
			ArrayList<Resources> resourcesToTradeSurvivor1, 
			ArrayList<Resources> resourcesToTradeSurvivor2)
	{
		getTrade().setIdSurvivor1(idSurvivor1);
		getTrade().setIdSurvivor2(idSurvivor2);
		getTrade().setResourcesToTradeSurvivor1(resourcesToTradeSurvivor1);
		getTrade().setResourcesToTradeSurvivor2(resourcesToTradeSurvivor2);
	}
	
	private boolean validTrade()
	{
		
		int pointsSurvivor1=calculaPoints(getTrade().getResourcesToTradeSurvivor1());
		int pointsSurvivor2=calculaPoints(getTrade().getResourcesToTradeSurvivor2());
		
		if(pointsSurvivor1==pointsSurvivor2)
		{
			return true;
		}
		else
			return false;
	}

	/*
	 * exchange not working
	 */
/*	private void exchange()
	{
		SurvivorsController s = new SurvivorsController();
		
		s.takeTradedResources(getTrade().getResourcesToTradeSurvivor1(), 
				getTrade().getIdSurvivor1());
		s.addTradedResources(getTrade().getResourcesToTradeSurvivor2(), 
				getTrade().getIdSurvivor1());
		
		s.takeTradedResources(getTrade().getResourcesToTradeSurvivor2(), 
				getTrade().getIdSurvivor2());
		s.addTradedResources(getTrade().getResourcesToTradeSurvivor1(), 
				getTrade().getIdSurvivor2());
	}*/
	
	public void confirmTrade()
	{
		if(validTrade())
		{
		//	exchange();
		}
	}
}
