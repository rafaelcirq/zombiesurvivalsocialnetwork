package br.com.ZombieSurvivalSocialNetwork.controller;

import java.util.ArrayList;

import br.com.ZombieSurvivalSocialNetwork.dao.SurvivorsDAO;
import br.com.ZombieSurvivalSocialNetwork.model.Resources;
import br.com.ZombieSurvivalSocialNetwork.model.Survivors;

/*
 * responsible for holding the necessary methods
 */
public class SurvivorsController {
	
	public ArrayList<Survivors> listAllSurvivors()
	{
		return SurvivorsDAO.getInstance().getSurvivors(null);
	}
	
	public void addTradedResources (ArrayList<Resources> tradedResources, int id)
	{
		SurvivorsDAO.getInstance().addResources(tradedResources, id);
	}
	
	public void takeTradedResources (ArrayList<Resources> tradedResources, int id)
	{
		SurvivorsDAO.getInstance().takeTradedResources(tradedResources, id);
	}
	
	public void updateSurvivorLocation(int id, String newLocation)
	{
		SurvivorsDAO.getInstance().updateSurvivorLocation(id, newLocation);
	}
	
	public void updateInfectedStatus(int id)
	{
		SurvivorsDAO.getInstance().updateInfectedStatus(id);
	}
	
	public void addNewSurvivor(Survivors newSurvivor)
	{
		SurvivorsDAO.getInstance().addNewSurvivor(newSurvivor);
	}
	
	public String getAverageAmountOfResources()
	{
		return SurvivorsDAO.getInstance().getAverageAmountsOfResource();
	}
	
	public void removeSurvivor(int id)
	{
		SurvivorsDAO.getInstance().removeSurvivor(id);
	}
	
	public String getPercentageOfInfectedSurvivors()
	{
		return String.valueOf(SurvivorsDAO.getInstance().percentageOfInfectedSurvivors());
	}
	
	public String getPercentageOfNonInfectedSurvivors()
	{
		return String.valueOf(SurvivorsDAO.getInstance().percentageOfNonInfectedSurvivors());
	}
}
