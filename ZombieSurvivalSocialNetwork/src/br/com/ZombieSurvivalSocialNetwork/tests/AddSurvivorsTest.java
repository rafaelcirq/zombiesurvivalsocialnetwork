package br.com.ZombieSurvivalSocialNetwork.tests;

import java.util.ArrayList;

import br.com.ZombieSurvivalSocialNetwork.controller.SurvivorsController;
import br.com.ZombieSurvivalSocialNetwork.model.Resources;
import br.com.ZombieSurvivalSocialNetwork.model.Survivors;

public class AddSurvivorsTest {
	public static void main(String[] args) {
		SurvivorsController s = new SurvivorsController();
		ArrayList<Resources> resourcesList = new ArrayList<>();
		resourcesList.add(Resources.FOOD);
		resourcesList.get(0).setQuantity(0);
		resourcesList.add(Resources.MEDICATION);
		resourcesList.get(1).setQuantity(0);
		resourcesList.add(Resources.AMMUNITION);
		resourcesList.get(2).setQuantity(0);
		resourcesList.add(Resources.WATER);
		resourcesList.get(3).setQuantity(40);
		
		Survivors survivor = new Survivors();
		survivor.setAge(30);
		survivor.setGender("Male");
		survivor.setInfected(false);
		survivor.setLocation("33.753746, -84.386330");
		survivor.setName("Daryl");
		
		survivor.setResourcesList(resourcesList);
		
		s.addNewSurvivor(survivor);
	}

}
