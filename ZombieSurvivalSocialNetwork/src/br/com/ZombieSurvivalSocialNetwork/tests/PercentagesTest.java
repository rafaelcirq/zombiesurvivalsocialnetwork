package br.com.ZombieSurvivalSocialNetwork.tests;

import br.com.ZombieSurvivalSocialNetwork.controller.SurvivorsController;

public class PercentagesTest {
	public static void main(String[] args) {
		SurvivorsController s = new SurvivorsController();
		
		System.out.println("Infected survivors: "+s.getPercentageOfInfectedSurvivors()+"%");
		System.out.println("Non-Infected survivors: "+s.getPercentageOfNonInfectedSurvivors()+"%");
	}

}
