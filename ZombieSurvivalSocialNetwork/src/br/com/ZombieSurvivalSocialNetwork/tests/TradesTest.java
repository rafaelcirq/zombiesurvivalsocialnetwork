package br.com.ZombieSurvivalSocialNetwork.tests;

import java.util.ArrayList;

import br.com.ZombieSurvivalSocialNetwork.controller.TradesController;
import br.com.ZombieSurvivalSocialNetwork.model.Resources;

public class TradesTest {
	
	public static void main(String[] args) {
		TradesController trades = new TradesController();
		
		ArrayList<Resources> resourcesToTradeSurvivor1 = new ArrayList<>();
		resourcesToTradeSurvivor1.add(Resources.MEDICATION);
		resourcesToTradeSurvivor1.get(0).setQuantity(2);
		
		ArrayList<Resources> resourcesToTradeSurvivor2 = new ArrayList<>();
		resourcesToTradeSurvivor2.add(Resources.WATER);
		resourcesToTradeSurvivor2.get(0).setQuantity(1);
		
		trades.setData(19, 20, resourcesToTradeSurvivor1, resourcesToTradeSurvivor2);
		
		trades.confirmTrade();
	}

}
